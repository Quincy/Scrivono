const { BrowserWindow, app, ipcMain } = require("electron")
require("electron-reloader")(module);

let mainWindow

const createWindow = () => {
    mainWindow = new BrowserWindow({
        width: 1200,
        height: 700,
        webPreferences: {
          nodeIntegration: true,
          contextIsolation: false,
          enableRemoteModule: true,
        }
    })

    mainWindow.loadFile("index.html")
    mainWindow.openDevTools();
};

app.whenReady().then(createWindow);
